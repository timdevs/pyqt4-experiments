# -*- encoding: utf-8 -*-
__author__ = 'maxim'
from time import sleep


class ManagedWindowMixin(object):

    def closeEvent(self, event):
        self.manager.windows.remove(self)
        event.accept()


class WindowManager(object):
    windows = []

    @classmethod
    def open(cls, module, title, size):

        class ManagedWindow(ManagedWindowMixin, module):
            pass
        ManagedWindow.__name__ = "Managed%s" % module.__name__

        def wrapper():
            w = ManagedWindow()
            w.setWindowTitle("%s #%s" % (title, len(cls.windows)))
            w.resize(*size)
            w.show()
            setattr(w, 'manager', cls)
            cls.windows.append(w)
        return wrapper
