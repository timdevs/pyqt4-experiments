# -*- encoding: utf-8 -*-
__author__ = 'maxim'

from PyQt4 import QtCore, QtGui
from manager import WindowManager
from random import randint
from time import sleep
import threading
import socket
import Queue


def create_sock_pair(port=0):
    """Create socket pair.

    If socket.socketpair isn't available, we emulate it.
    """
    # See if socketpair() is available.
    have_socketpair = hasattr(socket, 'socketpair')
    if have_socketpair:
        client_sock, srv_sock = socket.socketpair()
        return client_sock, srv_sock

    # Create a non-blocking temporary server socket
    temp_srv_sock = socket.socket()
    temp_srv_sock.setblocking(False)
    temp_srv_sock.bind(('', port))
    port = temp_srv_sock.getsockname()[1]
    temp_srv_sock.listen(1)

    # Create non-blocking client socket
    client_sock = socket.socket()
    client_sock.setblocking(False)
    try:
        client_sock.connect(('localhost', port))
    except socket.error as err:
        # EWOULDBLOCK is not an error, as the socket is non-blocking
        if err.errno != errno.EWOULDBLOCK:
            raise

    # Use select to wait for connect() to succeed.
    import select
    timeout = 1
    readable = select.select([temp_srv_sock], [], [], timeout)[0]
    if temp_srv_sock not in readable:
        raise Exception('Client socket not connected in {} second(s)'.format(timeout))
    srv_sock, _ = temp_srv_sock.accept()

    return client_sock, srv_sock


class OtherWindow(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.label = QtGui.QLabel(u"Динамически созданное окно")
        self.type_label = QtGui.QLabel(str(type(self)))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.type_label.setAlignment(QtCore.Qt.AlignCenter)
        self.btnOpen = QtGui.QPushButton(u"Открыть еще одно окно")
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.label)
        self.vbox.addWidget(self.type_label)
        self.vbox.addWidget(self.btnOpen)
        self.setLayout(self.vbox)
        self.connect(self.btnOpen, QtCore.SIGNAL("clicked()"), WindowManager.open(OtherWindow, u"Вікно", (500, 300)))


class SafeConnector:
    def __init__(self):
        # self._rsock, self._wsock = socket.socketpair()
        self._rsock, self._wsock = create_sock_pair()
        self._queue = Queue.Queue()
        self._qt_object = QtCore.QObject()
        self._notifier = QtCore.QSocketNotifier(self._rsock.fileno(),
                                                QtCore.QSocketNotifier.Read)
        self._notifier.activated.connect(self._recv)

    def connect(self, signal, receiver):
        QtCore.QObject.connect(self._qt_object, signal, receiver)

    # should be called by Python thread
    def emit(self, signal, args):
        self._queue.put((signal, args))
        self._wsock.send('!')

    # happens in Qt's main thread
    def _recv(self):
        self._rsock.recv(1)
        signal, args = self._queue.get()
        self._qt_object.emit(signal, args)



class PythonThread(threading.Thread):
    def __init__(self, connector, *args, **kwargs):
        threading.Thread.__init__(self, *args, **kwargs)
        self.connector = connector
        self.daemon = True

    def emit_signal(self, data):
        self.connector.emit(QtCore.SIGNAL("test"), str(data))

    def run(self):
        print("thread started.")
        for i in range(60,-1,-1):
            print "running... %s" % i
            sleep(0.25)
            self.emit_signal(i)
        self.emit_signal('stop')


class Driver(object):

    def __init__(self, connector):
        self.connector = connector
        self.python_thread = None

    def run(self):
        data = randint(1, 1000)
        print("random %s" % data)
        if data > 500:
            self.connector.emit(QtCore.SIGNAL("test"), str(data))
        else:
            self.start_python_thread()

    def start_python_thread(self):
        self.python_thread = PythonThread(self.connector)
        self.python_thread.start()