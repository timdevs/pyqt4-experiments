# -*- encoding: utf-8 -*-
__author__ = 'maxim'


#####################


def mult(a):
    def inner(b):
        return a*b
    return inner


triple = mult(3)
print triple
print triple(2)


######################


def mult2(a):
    def inner(b):
        def inner2(c):
            return a*b*c
        return inner2
    return inner


t = mult2(3)
x = t(2)
print x(5)


print "---"
print mult2(2)(3)(4)