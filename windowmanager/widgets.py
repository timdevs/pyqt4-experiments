# -*- encoding: utf-8 -*-
from __future__ import print_function, division, unicode_literals, absolute_import
from PyQt4 import QtGui, QtCore


class Communicate(QtCore.QObject):
    updateW = QtCore.pyqtSignal(int)


class TimerWidget(QtGui.QWidget):
    def __init__(self, max_v=60, cur_v=None):
        super(TimerWidget, self).__init__()
        self.setMinimumSize(100, 100)
        self.setMaximumSize(100, 100)
        self.max_value = max_v
        self.value = cur_v or max_v

    def setValue(self, value):
        self.value = value

    @property
    def alen(self):
        return int(5760 * self.value / self.max_value)

    def paintEvent(self, e):
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing)
        self.drawWidget(qp)
        qp.end()

    def drawWidget(self, qp):
        conicalGradient = QtGui.QConicalGradient(50, 50, 150)
        conicalGradient.setColorAt(0.0, QtCore.Qt.darkGreen)
        conicalGradient.setColorAt(0.75, QtCore.Qt.green)
        conicalGradient.setColorAt(1.0, QtCore.Qt.white)
        qp.setFont(QtGui.QFont('Serif', 26))


        bgcolor = self.palette().color(QtGui.QWidget.backgroundRole(self))

        width = self.width()
        height = self.height()
        size = min([width, height]) - 1

        outer_topLeft = self.rect().topLeft()
        inner_topLeft = QtCore.QPoint(outer_topLeft.x() + 13, outer_topLeft.y() + 13)
        text_topLeft = QtCore.QPoint(outer_topLeft.x() + 30, outer_topLeft.y() + 30)

        contour_rect = QtCore.QRect(outer_topLeft, QtCore.QSize(size, size))
        outer_rect = QtCore.QRect(outer_topLeft, QtCore.QSize(size-1, size-1))
        inner_rect = QtCore.QRect(inner_topLeft, QtCore.QSize(size-26, size-26))
        inner_contour_rect = QtCore.QRect(inner_topLeft, QtCore.QSize(size-27, size-27))
        text_rect = QtCore.QRect(text_topLeft, QtCore.QSize(size-60, size-60))

        qp.setPen(QtGui.QPen(QtCore.Qt.white, 1))
        qp.drawEllipse(contour_rect)
        qp.setPen(QtGui.QPen(QtCore.Qt.gray, 1))
        qp.drawEllipse(inner_contour_rect )

        qp.setBrush(QtGui.QBrush(conicalGradient))
        qp.setPen(QtGui.QPen(QtCore.Qt.black, 0.01))

        qp.drawPie(outer_rect, 2400, self.alen)
        qp.setBrush(bgcolor)
        qp.drawEllipse(inner_rect)
        qp.drawText(text_rect, QtCore.Qt.AlignHCenter, str(self.value))


