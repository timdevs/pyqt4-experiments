# -*- encoding: utf-8 -*-
__author__ = 'maxim'


from lib import OtherWindow, Driver, PythonThread, SafeConnector
from widgets import TimerWidget, Communicate
from manager import WindowManager
from PyQt4 import QtCore, QtGui


class MyWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.label = QtGui.QLabel(u"Привет, мир!")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.thread_label = QtGui.QLabel(u"no data")
        self.thread_label.setAlignment(QtCore.Qt.AlignCenter)
        self.btnQuit = QtGui.QPushButton(u"&3акрыть окно")
        self.btnOpen = QtGui.QPushButton(u"Открыть еще одно окно")
        self.btnRun = QtGui.QPushButton(u"Запустить python thread")
        self.btnDrv = QtGui.QPushButton(u"Запустить python driver")
        self.timer = TimerWidget(cur_v=60)
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.label)
        self.vbox.addWidget(self.thread_label)
        self.vbox.addWidget(self.timer)
        self.vbox.addWidget(self.btnQuit)
        self.vbox.addWidget(self.btnOpen)
        self.vbox.addWidget(self.btnRun)
        self.vbox.addWidget(self.btnDrv)
        self.setLayout(self.vbox)
        self.connect(self.btnQuit, QtCore.SIGNAL("clicked()"), QtGui.qApp.quit)
        self.connect(self.btnOpen, QtCore.SIGNAL("clicked()"), WindowManager.open(OtherWindow, u"Вікно", (500, 300)))
        self.connect(self.btnRun, QtCore.SIGNAL("clicked()"), self.start_python_thread)

        self.connector = SafeConnector()
        self.connector.connect(QtCore.SIGNAL("test"), self.callback)

        self.driver = Driver(self.connector)
        self.connect(self.btnDrv, QtCore.SIGNAL("clicked()"), self.driver.run)
        self.python_thread = None

        self.c = Communicate()
        self.c.updateW[int].connect(self.timer.setValue)

    def callback(self, data):
        if data == 'stop':
            self.wait_python_thread_terminate()
        else:
            self.thread_label.setText(data)
            self.c.updateW.emit(int(data))
            self.timer.repaint()

    def start_python_thread(self):
        self.python_thread = PythonThread(self.connector)
        print("starting thread...")
        self.python_thread.start()

    def wait_python_thread_terminate(self):
        print("waiting for thread stop...")
        if self.python_thread:
            self.python_thread.join()
        print("thread stopped.")
        self.python_thread = None


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    window = MyWindow() # Создаем экземпляр класса
    window.setWindowTitle(u"ООП-стиль создания окна")
    window.resize(300, 70)
    window.show()
    sys.exit(app.exec_())